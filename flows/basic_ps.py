from prefect import Flow, Parameter, unmapped

from flows.winrm_task import winrm_powershell


with Flow(" basic powershell") as flow:
    hostname = Parameter('hostname')
    command = Parameter('command')
    crt_file_name = Parameter('crt_file_name')
    key_file_name = Parameter('key_file_name')
    results = winrm_powershell.map(hostname=hostname,
                             command=unmapped(command),
                             crt_file_name=unmapped(crt_file_name),
                             key_file_name=unmapped(key_file_name))


# This is in here so we can access the magic 'results' object
def get_results(state):
    return state.result[results].result
