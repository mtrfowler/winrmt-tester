import sys
from base64 import b64encode
from prefect import task

import winrm


@task
def winrm_powershell(hostname, crt_file_name, key_file_name, command):
    pp = winrm.Protocol(
        endpoint=f'https://{hostname}:5986/wsman',
        transport='certificate',
        cert_pem=crt_file_name,
        cert_key_pem=key_file_name,
        server_cert_validation='ignore')

    shell_id = pp.open_shell()
    encoded_ps = b64encode(command.encode('utf_16_le')).decode('ascii')
    # command_id = pp.run_command(shell_id, *command)
    command_id = pp.run_command(shell_id, 'powershell -encodedcommand {0}'.format(encoded_ps))
    std_out, std_err, status_code = pp.get_command_output(shell_id, command_id)
    if std_err:
        print("std_err:", str(std_err).replace("\\r\\n", "\n"), file=sys.stderr)
    pp.cleanup_command(shell_id, command_id)
    pp.close_shell(shell_id)
    return std_out.decode('UTF-8')
