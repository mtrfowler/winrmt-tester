# README #


### What is this repository for? ###

* Simple winrm library test.

### How do I get set up? ###
 This will use the MCS version of pywinrm with the loop timeout not present in the master.
 
```
virtualenv -p python3 venv
. venv/bin/activate
pip install -r requirements.txt
```

#### run with
  python ./winrmt.py --user windows_user --password windows_password  -w windoz.blah.com
