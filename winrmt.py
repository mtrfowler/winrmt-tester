#!/usr/bin/env python
import sys
import os
import argparse
import tempfile
import importlib
from base64 import b64encode
from multiprocessing.pool import ThreadPool

from mcsjwt import key_reader
from mcsjwt.schemas import awx_schema

from prefect.engine.executors import LocalDaskExecutor

import vault_utils

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='test pywinrm')
    parser.add_argument("-w", "--win-host", required=True, help="windows host")
    parser.add_argument("--flow", help="Run a prefect flow")
    parser.add_argument("--flow-param", help="string passed to flow")
    # Standard MCS vault arguments
    parser.add_argument("-O", "--org", dest="org", default="mcs",
                        help="Organization - the root of our secrets hierarchy")
    parser.add_argument("-s", "--stage", dest='stage', default=os.getenv("STAGE", "lab"),
                        help="Defines our operating environment (lab, prod, mylab', etc,). "
                             "Stage is the second part of secrets hierarchy.")
    vault_utils.get_opts(parser)
    opts = parser.parse_args()

    if not opts.win_host:
        print("needs -w <host>")
        sys.exit(1)

    vault_client = vault_utils.connect(opts)
    keys = key_reader.read_yaml_vault(vault_client, 'awx', stage=opts.stage, schema=awx_schema)
    # This is an unacceptable hack but requests does not accept keys from memory/strings
    # so we have to save them,
    winrm_crt_file = tempfile.NamedTemporaryFile()
    winrm_key_file = tempfile.NamedTemporaryFile()
    winrm_crt_file.write(bytearray(keys.winrm_crt, 'utf-8'))
    winrm_crt_file.file.flush()
    winrm_key_file.write(bytearray(keys.winrm_key, 'utf-8'))
    winrm_key_file.file.flush()

    command = r"""
        Start-Sleep 4
        $result = New-Object psobject @{
        packages = "Packages"
        windows_edition = "MSDOS3.1"
        }
        Write-OutPut $result
        """
    # command = b64encode(command.encode('UTF-16LE'))
#    command = 'ipconfig /all'
    if opts.flow:
        flow = importlib.import_module(opts.flow)
        executor = LocalDaskExecutor(pool=ThreadPool(), num_workers=40)
        state = flow.flow.run(executor=executor,
                         hostname=[opts.win_host],
                         command=command,
                         crt_file_name=winrm_crt_file.name,
                         key_file_name=winrm_key_file.name)
        print(flow.get_results(state)[0])
