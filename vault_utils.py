import os
import hvac

def get_opts(parser):
    parser.add_argument("-V", "--vault-uri", dest='vault_uri',
                        default=os.getenv("VAULT_ADDR", "http://vault:8200"),
                        help="vault URI")
    parser.add_argument("-R", "--cert-file", dest='cert_file', default=os.getenv("VAULT_CACERT"),
                        help="CA cert file")
    parser.add_argument("-t", "--token", dest='token', default=os.getenv("VAULT_TOKEN"),
                        help="vault token")

def connect(opts):
    """ Open the connection to vault using the client library.
        Raises exception on failure.
    """
    if opts.cert_file:
        os.environ['REQUESTS_CA_BUNDLE'] = opts.cert_file
    client = hvac.Client(url=opts.vault_uri, token=opts.token)
    try:
        client.is_authenticated()
    except Exception as excp:
        raise Exception(f"Can't connect to secrets vault {opts.vault_uri} error {str(excp)}")
    return client

